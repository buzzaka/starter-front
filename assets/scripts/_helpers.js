// port underscore findWhere
//
// findWhere_.findWhere(list, {'key' : value })
//
// Looks through the list and returns the first value that matches all of the key-value pairs listed in properties.
// If no match is found, or if list is empty, undefined will be returned.
var _h =
{
    findWhere: function (list, props)
    {
        var idx = 0;
        var len = list.length;
        var match = false;
        var item, item_k, item_v, prop_k, prop_val;

        for (; idx<len; idx++) {
            item = list[idx];
            for (prop_k in props) {
                // If props doesn't own the property, skip it.
                if (!props.hasOwnProperty(prop_k)) {
                    continue;
                }
                // If item doesn't have the property, no match;
                if (!item.hasOwnProperty(prop_k)) {
                    match = false;
                    break;
                }
                if (props[prop_k] === item[prop_k]) {
                    // We have a match…so far.
                    match = true;
                } else {
                    // No match.
                    match = false;
                    // Don't compare more properties.
                    break;
                }
            }

            // We've iterated all of props' properties, and we still match!
            // Return that item!
            if (match) {
                return item;
            }
        }
        // No matches
        return null;
    },
    // Get param from url
    // similar to php $_GET
    $_GET: function(param) {
        var vars = {};
        window.location.href.replace(
            /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
            function( m, key, value ) { // callback
                vars[key] = value !== undefined ? value : '';
            }
        );

        if ( param ) {
            return vars[param] ? vars[param] : null;
        }
        return vars;
    },
    // Lazy scroll to an element in a page
    scrollTo: function( element ){
        $('html, body').animate({
            scrollTop: $(element).offset().top
        }, 800);
    }
};