<?php
/**
 * Scripts and stylesheets
 *
 * Enqueue stylesheets in the following order:
 * 1. /theme/assets/styles/main.css
 *
 * Enqueue scripts in the following order:
 * 1. jquery-2.2.0.min.js via Google CDN
 * 2. /theme/assets/scripts/vendor/modernizr.min.js
 * 3. /theme/assets/scripts/main.js (in footer)
 *
 * Google Analytics is loaded after enqueued scripts if:
 * - An ID has been defined in config.php
 * - You're not logged in as an administrator
 */
function app_scripts() {
  /**
   * The build task in Grunt renames production assets with a hash
   * Read the asset names from assets-manifest.json
   */
  if (APP_ENV === 'development') {
    $assets = array(
      'css'       => './dist/styles/main.css',
      'js'        => './dist/scripts/main.js',
      'modernizr' => './dist/scripts/modernizr.js',
      'jquery'    => '//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.js'
    );
  } else {
    $get_assets = file_get_contents(__ROOT__. '/assets/manifest.json');
    $assets     = json_decode($get_assets, true);
    $assets     = array(
      'css'       => './dist/styles/main.min.css?' . $assets['assets/styles/main.min.css']['hash'],
      'js'        => './dist/scripts/main.min.js?' . $assets['assets/scripts/main.min.js']['hash'],
      'modernizr' => './dist/scripts/modernizr.min.js',
      'jquery'    => '//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js'
    );
  }

  return $assets;
}

function app_css(){
  $assets = app_scripts();
  echo '<link rel="stylesheet" href="'.$assets['css'].'">'.PHP_EOL;
}

function app_head_js(){
  $assets = app_scripts();
}

function app_footer_js(){
  $assets = app_scripts();
  echo <<<SCRIPT
    <script src="{$assets['jquery']}"></script>
    <script>window.jQuery || document.write('<script src="./assets/vendor/jquery/dist/jquery.min.js?2.2.0"><\/script>')</script>
    <script src="{$assets['js']}"></script>
SCRIPT;
}


/**
 * Google Analytics snippet from HTML5 Boilerplate
 *
 * Cookie domain is 'auto' configured. See: http://goo.gl/VUCHKM
 */
function app_google_analytics() { ?>
<script>
  <?php if (APP_ENV === 'production') : ?>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  <?php else : ?>
    function ga() {
      console.log('GoogleAnalytics: ' + [].slice.call(arguments));
    }
  <?php endif; ?>
  ga('create','<?php echo GOOGLE_ANALYTICS_ID; ?>','auto');ga('send','pageview');
</script>

<?php }
