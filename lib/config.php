<?php

/**
 * Configuration values
 */
define('GOOGLE_ANALYTICS_ID', ''); // UA-XXXXX-Y (Note: Universal Analytics only, not Classic Analytics)
define('APP_ENV', 'development');


if (!defined('APP_ENV')) {
  define('APP_ENV', 'production');  // scripts.php checks for values 'production' or 'development'
}