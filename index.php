<?php require dirname(__FILE__).'/lib/start.php' ?>
<!DOCTYPE html>
<html class="no-js sticky-footer" lang="fr-FR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dev Base Front</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
    <?php app_css(); ?>
    <?php app_head_js(); ?>
    </head>
    <body class="home page sidebar-primary">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header class="banner navbar navbar-default navbar-static-top" role="banner">
            <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./">Starter</a>
            </div>

                <nav class="collapse navbar-collapse" role="navigation">
                    <ul id="menu-primary-navigation" class="nav navbar-nav">
                        <li class="active menu-home"><a href="./" title="Home">Home</a></li>
                        <li class="menu-sample-page"><a href="./" title="Sample Page">Sample Page</a></li>
                    </ul>
                </nav>
            </div>
        </header>

        <div class="wrap container" role="document">
            <div class="content row">
                <main class="main" role="main">
                    <div class="page-header">
                      <?php include 'readme.php'; ?>
                    </div>
                </main>
                <aside class="sidebar" role="complementary">
                    <section class="widget search-2 widget_search">
                        <form role="search" method="get" class="search-form form-inline" action="http://127.0.0.1/~Mathias/bedrock/web/">
                            <label class="sr-only">Search for:</label>
                            <div class="input-group">
                                <input type="search" value="" name="s" class="search-field form-control" placeholder="Search Bedrock">
                                <span class="input-group-btn">
                                    <button type="submit" class="search-submit btn btn-default">Search</button>
                                </span>
                            </div>
                        </form>
                    </section>
                </aside>
            </div>
        </div>

        <footer class="footer content-info" role="contentinfo">
          <div class="container">
            <p class="text-muted">© Starter Theme</p>
          </div>
        </footer>

        <!-- Footer scripts -->
        <?php app_footer_js(); ?>
        <?php app_google_analytics(); ?>
    </body>
</html>