<h1>[Dev Front Starter]</h1>

<p>This starter is a mixed of the Roots starter theme for wordpress and Html5 Boilerplate.
See below for more informations</p>

<h2>Requirements :</h2>

<ul>
    <li><code>nodejs</code></li>
    <li><code>gulp</code></li>
    <li><code>bower</code></li>
</ul>


<h2>Features</h2>

<ul>
    <li><a href="http://gulpjs.com/">gulp</a> build script that compiles both Sass and Less, checks for JavaScript errors, optimizes images, and concatenates and minifies files</li>
    <li><a href="http://www.browsersync.io/">BrowserSync</a> for keeping multiple browsers and devices synchronized while testing, along with injecting updated CSS and JS into your browser while you're developing</li>
    <li><a href="http://bower.io/">Bower</a> for front-end package management</li>
    <li><a href="https://github.com/austinpray/asset-builder">asset-builder</a> for the JSON file based asset pipeline</li>
    <li><a href="http://getbootstrap.com/">Bootstrap</a></li>
</ul>


<h2>Configuration</h2>

<p>You'll need to add the following to your <code>lib/config.php</code> on your development installation:</p>

<p><code>php
define('WP_ENV', 'development');
</code></p>

<p>Edit <code>lib/config.php</code> to enable or disable theme features and to define a Google Analytics ID.</p>

<h2>Theme development</h2>

<p>Sage uses <a href="http://gulpjs.com/">gulp</a> as its build system and <a href="http://bower.io/">Bower</a> to manage front-end packages.</p>

<h3>Install gulp and Bower</h3>

<p>Building the theme requires <a href="http://nodejs.org/download/">node.js</a>. We recommend you update to the latest version of npm: <code>npm install -g npm@latest</code>.</p>

<p>From the command line:</p>

<ol>
<li>Install <a href="http://gulpjs.com">gulp</a> and <a href="http://bower.io/">Bower</a> globally with <code>npm install -g gulp bower</code></li>
<li>Navigate to the theme directory, then run <code>npm install</code></li>
<li>Run <code>bower install</code></li>
</ol>

<p>You now have all the necessary dependencies to run the build process.</p>

<h3>Available gulp commands</h3>

<ul>
<li><code>gulp</code> — Compile and optimize the files in your assets directory</li>
<li><code>gulp watch</code> — Compile assets when file changes are made</li>
<li><code>gulp --production</code> — Compile assets for production (no source maps).</li>
</ul>

<h3>Using BrowserSync</h3>

<p>To use BrowserSync during <code>gulp watch</code> you need to update <code>devUrl</code> at the bottom of <code>assets/manifest.json</code> to reflect your local development hostname.</p>

<p>For example, if your local development URL is <code>http://project-name.dev</code> you would update the file to read:</p>

<div class="highlight highlight-source-json"><pre>...
  <span class="pl-s"><span class="pl-pds">"</span>config<span class="pl-pds">"</span></span>: {
    <span class="pl-s"><span class="pl-pds">"</span>devUrl<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>http://project-name.dev<span class="pl-pds">"</span></span>
  }
...</pre></div>

<p>If your local development URL looks like <code>http://localhost:8888/project-name/</code> you would update the file to read:</p>

<div class="highlight highlight-source-json"><pre>...
  <span class="pl-s"><span class="pl-pds">"</span>config<span class="pl-pds">"</span></span>: {
    <span class="pl-s"><span class="pl-pds">"</span>devUrl<span class="pl-pds">"</span></span>: <span class="pl-s"><span class="pl-pds">"</span>http://localhost:8888/project-name/<span class="pl-pds">"</span></span>
  }
...</pre></div>

<ul>
    <li>Source: <a href="https://github.com/h5bp/html5-boilerplate">https://github.com/h5bp/html5-boilerplate</a></li>
    <li>Homepage: <a href="http://html5boilerplate.com">http://html5boilerplate.com</a></li>
    <li>Twitter: <a href="http://twitter.com/h5bp">@h5bp</a></li>
</ul>